/// <reference path="../typings/tsd.d.ts" />
import Rx = require("rx");
import logs = require("da-logs");
import rabbit = require("da-rabbitmq-rx");
var uuid = require("node-uuid");
import trAnt =require("tr-ant-utils");
import tn = require("da-trader-net-rx");

import tulpe = trAnt.tulpe;

const PKG_NAME = require("../package.json").name;

var newDate = () => new Date();  
var newUUID = () => uuid.v4();

export interface IHandlerStorage {
    lock(key: string) : Rx.Observable<boolean>
    insertKey(key: string) : Rx.Observable<number>  
}

export interface IHandlerOpts {
  portfolio: string
	cmdStream: Rx.Observable<trAnt.ICmd>
	logger: logs.ILogger 	  
	newDate?() : Date
	newUUID?() : string
  putOrder(order: tn.IPutOrderData) : void
  storage : IHandlerStorage
}

interface ICmdOrder {
  id : number  
  cmd: trAnt.ICmd
} 

function getOrderMeta(cmd : trAnt.ICmd, storage: IHandlerStorage) : Rx.Observable<ICmdOrder> {  
  return storage.insertKey(cmd.key).map((id, portf) => {
    return {
      id : id,
      cmd: cmd
    }
  }); 
} 

function mapCommand(order: ICmdOrder) : tn.IPutOrderData {
  return {
        ticket: order.cmd.trade.ticket,
        action: order.cmd.trade.oper == "buy" ? tn.OrderActionTypes.Buy : tn.OrderActionTypes.Sell,
        orderType: tn.OrderTypes.Market,
        currency: tn.CurrencyCodes.RUR,
        quantity: order.cmd.trade.quantity,
        userOrderId: order.id.toString()
  }
}

export function handle(opts : IHandlerOpts) {
	if (opts.newDate)
		newDate = opts.newDate;
	if (opts.newUUID)		
		newUUID = opts.newUUID;
    
  var lock = (key: string) => opts.storage.lock(key);
        
  opts.cmdStream//.filter(cmd => cmd.portfolio == opts.portfolio)
  .do(val => opts.logger.write({oper: "exe", status: "start", data : { cmd: val } }))
  .flatMap(val => lock(val.key).map(l => tulpe(l, val)))
  .do(tlp => {
    if (!tlp.val1)
      opts.logger.write({oper: "exe", status: "cancel", data : { cmd: tlp.val2 } })
   })
  .filter(tlp => tlp.val1)  
  .flatMap(tlp => getOrderMeta(tlp.val2, opts.storage))       
  .map(val => tulpe(val, mapCommand(val)))
  .subscribe(tlp => {
        opts.putOrder(tlp.val2);
        opts.logger.write({oper: "exe", status: "success", data : { cmd: tlp.val1, order: tlp.val2 } }); 
  });    
}


