/// <reference path="../typings/tsd.d.ts" />
import Rx = require("rx");
import logs = require("da-logs");
import rabbit = require("da-rabbitmq-rx");
var uuid = require("node-uuid");
import trAnt =require("tr-ant-utils");
import tn = require("da-trader-net-rx");

const PKG_NAME = require("../package.json").name;

interface IPortfChangedItem   {
  portf: tn.ITraderNetPortfolio
  notif : trAnt.INotif<trAnt.INotifPortfolioChanged>
}

var newDate = () => new Date();  
var newUUID = () => uuid.v4();

function mapPosition(pos: tn.ITraderNetPosition) : trAnt.IPortfolioPosition {
  return {
      ticket: tn.TicketCodes[pos.security],
      quantity: pos.quantity,
      value: pos.price    
  }
}

export function mapPortfolio(portf: tn.ITraderNetPortfolio, portfolio: string): trAnt.INotif<trAnt.INotifPortfolioChanged> {
    
    var positions = portf.positions ? portf.positions.map(mapPosition) : [];        
                                        
    var notif : trAnt.INotif<trAnt.INotifPortfolioChanged> = {
        key: newUUID(),
        type: "INotifPortfolioChanged",
        issuer: PKG_NAME,
        date : newDate().toISOString(),                     
        data : {
            portfolio: portfolio,
            value: portf.accounts && portf.accounts.length ? portf.accounts[0].availableAmount : null,
            positions: positions,
            cmd: null             
        }       
    };           
        
    return notif;    
}

export interface IHandlerOpts {
  portfolio: string
	portfChangedStream: Rx.Observable<tn.ITraderNetPortfolio>
  requestPortfStream: Rx.Observable<trAnt.INotifRequest<trAnt.INotifRequestPortfolioQuery>>
  pub: rabbit.RabbitPub 
	logger: logs.ILogger 	  
	newDate?() : Date
	newUUID?() : string
}

export function handle(opts : IHandlerOpts) {
	if (opts.newDate)
		newDate = opts.newDate;
	if (opts.newUUID)		
		newUUID = opts.newUUID;
            
  var portfChangedStream = opts.portfChangedStream
  .do(val => opts.logger.write({oper: "exe", status: "start", group: "notif", data : { portf: val } }))         
  .map<IPortfChangedItem>(val => { return {portf : val, notif : mapPortfolio(val, opts.portfolio)}} )
  .shareReplay(1);
  
  opts.requestPortfStream
  .filter(f => f.query.portfolio == opts.portfolio)
  .do(val => opts.logger.write({oper: "exe", status: "start", group: "req", data : { req : val } }))
  .withLatestFrom(portfChangedStream, (req, portf) => trAnt.tulpe(req, portf))
  .subscribe(tlp => {
      opts.pub.write(tlp.val2.notif);
      opts.logger.write({oper: "exe", status: "success", group: "req", data : {req : tlp.val1, notif : tlp.val2.notif, portf : tlp.val2.portf} });     
  });
  
  //opts.requestPortfStream.subscribe(trAnt.createObserver("requestPortfStream"));
  
  portfChangedStream.subscribe(val => {
        opts.pub.write(val.notif);
        opts.logger.write({oper: "exe", status: "success", group: "notif", data : val }); 
  });    
}
