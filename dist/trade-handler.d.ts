/// <reference path="../typings/tsd.d.ts" />
import Rx = require("rx");
import logs = require("da-logs");
import trAnt = require("tr-ant-utils");
import tn = require("da-trader-net-rx");
export interface IHandlerStorage {
    lock(key: string): Rx.Observable<boolean>;
    insertKey(key: string): Rx.Observable<number>;
}
export interface IHandlerOpts {
    portfolio: string;
    cmdStream: Rx.Observable<trAnt.ICmd>;
    logger: logs.ILogger;
    newDate?(): Date;
    newUUID?(): string;
    putOrder(order: tn.IPutOrderData): void;
    storage: IHandlerStorage;
}
export declare function handle(opts: IHandlerOpts): void;
