/// <reference path="../typings/tsd.d.ts" />
import Rx = require("rx");
import logs = require("da-logs");
import rabbit = require("da-rabbitmq-rx");
import trAnt = require("tr-ant-utils");
import tn = require("da-trader-net-rx");
export declare function mapPortfolio(portf: tn.ITraderNetPortfolio, portfolio: string): trAnt.INotif<trAnt.INotifPortfolioChanged>;
export interface IHandlerOpts {
    portfolio: string;
    portfChangedStream: Rx.Observable<tn.ITraderNetPortfolio>;
    requestPortfStream: Rx.Observable<trAnt.INotifRequest<trAnt.INotifRequestPortfolioQuery>>;
    pub: rabbit.RabbitPub;
    logger: logs.ILogger;
    newDate?(): Date;
    newUUID?(): string;
}
export declare function handle(opts: IHandlerOpts): void;
