var uuid = require("node-uuid");
var trAnt = require("tr-ant-utils");
var tn = require("da-trader-net-rx");
var tulpe = trAnt.tulpe;
var PKG_NAME = require("../package.json").name;
var newDate = function () { return new Date(); };
var newUUID = function () { return uuid.v4(); };
function getOrderMeta(cmd, storage) {
    return storage.insertKey(cmd.key).map(function (id, portf) {
        return {
            id: id,
            cmd: cmd
        };
    });
}
function mapCommand(order) {
    return {
        ticket: order.cmd.trade.ticket,
        action: order.cmd.trade.oper == "buy" ? tn.OrderActionTypes.Buy : tn.OrderActionTypes.Sell,
        orderType: tn.OrderTypes.Market,
        currency: tn.CurrencyCodes.RUR,
        quantity: order.cmd.trade.quantity,
        userOrderId: order.id.toString()
    };
}
function handle(opts) {
    if (opts.newDate)
        newDate = opts.newDate;
    if (opts.newUUID)
        newUUID = opts.newUUID;
    var lock = function (key) { return opts.storage.lock(key); };
    opts.cmdStream
        .do(function (val) { return opts.logger.write({ oper: "exe", status: "start", data: { cmd: val } }); })
        .flatMap(function (val) { return lock(val.key).map(function (l) { return tulpe(l, val); }); })
        .do(function (tlp) {
        if (!tlp.val1)
            opts.logger.write({ oper: "exe", status: "cancel", data: { cmd: tlp.val2 } });
    })
        .filter(function (tlp) { return tlp.val1; })
        .flatMap(function (tlp) { return getOrderMeta(tlp.val2, opts.storage); })
        .map(function (val) { return tulpe(val, mapCommand(val)); })
        .subscribe(function (tlp) {
        opts.putOrder(tlp.val2);
        opts.logger.write({ oper: "exe", status: "success", data: { cmd: tlp.val1, order: tlp.val2 } });
    });
}
exports.handle = handle;
