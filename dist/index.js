/// <reference path="../typings/tsd.d.ts" />
var rabbit = require("da-rabbitmq-rx");
var logs = require("da-logs");
var tradeHandler = require("./trade-handler");
var portfChangedHandler = require("./portf-changed-handler");
var trAnt = require("tr-ant-utils");
var mongo = require("da-mongo-rx");
var tn = require("da-trader-net-rx");
var PKG_NAME = require("../package.json").name;
var getEnvVar = trAnt.getEnvVar;
var RABBIT_URI = getEnvVar("RABBIT_URI");
var RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS");
var RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS");
var RABBIT_QUEUE_QUOTES = getEnvVar("RABBIT_QUEUE_QUOTES");
var LOG_LOGGLY_KEY = getEnvVar("LOG_LOGGLY_KEY");
var LOG_LOGGLY_SUBDOMAIN = getEnvVar("LOG_LOGGLY_SUBDOMAIN");
var LOG_MONGO_URI = getEnvVar("LOG_MONGO_URI");
var LOG_MONGO_COLLECTION = getEnvVar("LOG_MONGO_COLLECTION");
var MONGO_URI = getEnvVar("MONGO_URI");
var MONGO_LOCK_COLLECTION = getEnvVar("EXR_TRADER_NET_MONGO_LOCK_COLLECTION");
var MONGO_TRADE_KEYS_COLLECTION = getEnvVar("EXR_TRADER_NET_MONGO_TRADE_KEYS_COLLECTION");
var PORTFOLIO = getEnvVar("EXR_TRADER_NET_PORTFOLIO");
var TRADER_NET_URL = getEnvVar("TRADER_NET_URL");
var TRADER_NET_API_KEY = getEnvVar("TRADER_NET_API_KEY");
var TRADER_NET_SEC_KEY = getEnvVar("TRADER_NET_SEC_KEY");
var RABBIT_QUEUE_REQUEST_NOTIF = getEnvVar("RABBIT_QUEUE_REQUEST_NOTIF");
var logger = new logs.LoggerCompose({ pack: require("../package.json"), tags: [] }, {
    loggly: { token: LOG_LOGGLY_KEY, subdomain: LOG_LOGGLY_SUBDOMAIN },
    mongo: { connection: LOG_MONGO_URI, collection: LOG_MONGO_COLLECTION },
    console: true
});
logger.write({ status: "config",
    RABBIT_URI: RABBIT_URI,
    RABBIT_QUEUE_CMDS: RABBIT_QUEUE_CMDS,
    RABBIT_QUEUE_NOTIFS: RABBIT_QUEUE_NOTIFS,
    RABBIT_QUEUE_QUOTES: RABBIT_QUEUE_QUOTES,
    LOG_LOGGLY_KEY: LOG_LOGGLY_KEY,
    LOG_LOGGLY_SUBDOMAIN: LOG_LOGGLY_SUBDOMAIN,
    LOG_MONGO_URI: LOG_MONGO_URI,
    LOG_MONGO_COLLECTION: LOG_MONGO_COLLECTION,
    MONGO_URI: MONGO_URI,
    MONGO_LOCK_COLLECTION: MONGO_LOCK_COLLECTION,
    MONGO_TRADE_KEYS_COLLECTION: MONGO_TRADE_KEYS_COLLECTION,
    PORTFOLIO: PORTFOLIO,
    TRADER_NET_URL: TRADER_NET_URL,
    TRADER_NET_API_KEY: TRADER_NET_API_KEY,
    TRADER_NET_SEC_KEY: TRADER_NET_SEC_KEY,
    RABBIT_QUEUE_REQUEST_NOTIF: RABBIT_QUEUE_REQUEST_NOTIF
});
var db = new mongo.MongoDb(MONGO_URI, [MONGO_LOCK_COLLECTION, MONGO_TRADE_KEYS_COLLECTION]);
var cmdSubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_CMDS };
var cmdSub = new rabbit.RabbitSub(cmdSubOpts);
cmdSub.connect();
cmdSub.stream.take(1).subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: cmdSubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", status: "error", err: err, opts: cmdSubOpts });
    process.exit(1);
});
var traderNet = new tn.TraderNet(TRADER_NET_URL);
var tnConnectStream = traderNet.connect({ apiKey: TRADER_NET_API_KEY, securityKey: TRADER_NET_SEC_KEY });
tnConnectStream.subscribe(function () {
    console.log({ resource: "trader-net", oper: "connected", status: "success", opts: TRADER_NET_URL });
    traderNet.startRecievePortfolio();
}, function (err) {
    console.log({ resource: "trader-net", status: "error", err: err, opts: TRADER_NET_URL });
});
var tradeHandlerOpts = {
    cmdStream: cmdSub.stream.skip(1),
    portfolio: PORTFOLIO,
    logger: logger,
    putOrder: function (order) { return traderNet.putOrder(order); },
    storage: {
        lock: function (key) { return db.lock(key, MONGO_LOCK_COLLECTION); },
        insertKey: function (key) { return db.insertUniqueDocumentWithKey(key, MONGO_TRADE_KEYS_COLLECTION); }
    }
};
tradeHandler.handle(tradeHandlerOpts);
var reqSubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_REQUEST_NOTIF };
var reqSub = new rabbit.RabbitSub(reqSubOpts);
reqSub.connect();
reqSub.stream.take(1).subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: reqSubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", oper: "connected", status: "error", err: err, opts: reqSubOpts });
    process.exit(1);
});
var notifsPubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_NOTIFS };
var notifsPub = new rabbit.RabbitPub(notifsPubOpts);
notifsPub.connect();
notifsPub.connectStream.subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: notifsPubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", status: "error", err: err, opts: notifsPubOpts });
    process.exit(1);
});
var portfChangedOpts = {
    logger: logger,
    pub: notifsPub,
    portfolio: PORTFOLIO,
    portfChangedStream: traderNet.portfolioStream,
    requestPortfStream: reqSub.stream.skip(1)
};
portfChangedHandler.handle(portfChangedOpts);
