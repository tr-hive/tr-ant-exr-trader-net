var uuid = require("node-uuid");
var trAnt = require("tr-ant-utils");
var tn = require("da-trader-net-rx");
var PKG_NAME = require("../package.json").name;
var newDate = function () { return new Date(); };
var newUUID = function () { return uuid.v4(); };
function mapPosition(pos) {
    return {
        ticket: tn.TicketCodes[pos.security],
        quantity: pos.quantity,
        value: pos.price
    };
}
function mapPortfolio(portf, portfolio) {
    var positions = portf.positions ? portf.positions.map(mapPosition) : [];
    var notif = {
        key: newUUID(),
        type: "INotifPortfolioChanged",
        issuer: PKG_NAME,
        date: newDate().toISOString(),
        data: {
            portfolio: portfolio,
            value: portf.accounts && portf.accounts.length ? portf.accounts[0].availableAmount : null,
            positions: positions,
            cmd: null
        }
    };
    return notif;
}
exports.mapPortfolio = mapPortfolio;
function handle(opts) {
    if (opts.newDate)
        newDate = opts.newDate;
    if (opts.newUUID)
        newUUID = opts.newUUID;
    var portfChangedStream = opts.portfChangedStream
        .do(function (val) { return opts.logger.write({ oper: "exe", status: "start", group: "notif", data: { portf: val } }); })
        .map(function (val) { return { portf: val, notif: mapPortfolio(val, opts.portfolio) }; })
        .shareReplay(1);
    opts.requestPortfStream
        .filter(function (f) { return f.query.portfolio == opts.portfolio; })
        .do(function (val) { return opts.logger.write({ oper: "exe", status: "start", group: "req", data: { req: val } }); })
        .withLatestFrom(portfChangedStream, function (req, portf) { return trAnt.tulpe(req, portf); })
        .subscribe(function (tlp) {
        opts.pub.write(tlp.val2.notif);
        opts.logger.write({ oper: "exe", status: "success", group: "req", data: { req: tlp.val1, notif: tlp.val2.notif, portf: tlp.val2.portf } });
    });
    portfChangedStream.subscribe(function (val) {
        opts.pub.write(val.notif);
        opts.logger.write({ oper: "exe", status: "success", group: "notif", data: val });
    });
}
exports.handle = handle;
