/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/trade-handler.d.ts"/>
/// <reference path="../dist/portf-changed-handler.d.ts"/>
import chai = require('chai');
import portfChanged = require('../dist/portf-changed-handler');
import rabbit = require('da-rabbitmq-rx');
import trAnt = require("tr-ant-utils");
import tn = require("da-trader-net-rx");
import Rx = require("rx");
var expect = chai.expect;
import sinon = require("sinon");
import sinonChai = require("sinon-chai");
import mongo = require("da-mongo-rx");

chai.use(sinonChai);

import getEnvVar = trAnt.getEnvVar;

const TEST_IDX = 3;
const RABBIT_URI = getEnvVar("RABBIT_URI_TEST");
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS") + TEST_IDX;
const RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS") + TEST_IDX;

const TRADER_NET_URL = getEnvVar("TRADER_NET_URL_TEST");
const TRADER_NET_API_KEY = getEnvVar("TRADER_NET_API_KEY_TEST");
const TRADER_NET_SEC_KEY = getEnvVar("TRADER_NET_SEC_KEY_TEST");

const LOCK_COLL = "lock" + TEST_IDX;
const KEYS_COLL = "trade_keys" + TEST_IDX;

const MONGO_URI = getEnvVar("MONGO_URI_TEST");

console.log(RABBIT_URI, RABBIT_QUEUE_NOTIFS, RABBIT_QUEUE_CMDS);

//THIS TEST IS BRITTLE, DEPENDS ON TRADER_NET PORTFTOLIO 
//DEFAULT VALUES AND SHOULD BE INVOKED MANUALLY 

describe.skip("req portf test", () => {

	it("req portf test", (done) => {

		var traderNet = new tn.TraderNet(TRADER_NET_URL);
		var tnConnectStream = traderNet.connect({ apiKey: TRADER_NET_API_KEY, securityKey: TRADER_NET_SEC_KEY })

		tnConnectStream.subscribe(() =>
			console.log({ resource: "trader-net", oper: "connected", status: "success", opts: TRADER_NET_URL })
			, (err) => {
				console.log({ resource: "trader-net", status: "error", err: err, opts: TRADER_NET_URL });
			});
				
		//subscribe here and validate all publishing
		var subOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_CMDS };
		var sub = new rabbit.RabbitSub(subOpts);
		sub.connect();

		var notifsPubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_NOTIFS };
		var notifsPub = new rabbit.RabbitPub(notifsPubOpts); 
		notifsPub.connect();	
		var logger = {
			write(j) { console.log(JSON.stringify(j, null, 2)); }
		};		
		
		var reqStream = Rx.Observable.create<trAnt.INotifRequest<trAnt.INotifRequestPortfolioQuery>>(observer => {
			setTimeout(() => observer.onNext({type: "INotifPortfolioChanged", query : { portfolio: "test"}}), 500);
		});

		var portfChangedOpts: portfChanged.IHandlerOpts = {			
			logger: logger,
			pub: notifsPub,		
			portfolio: "test",	
			portfChangedStream: traderNet.portfolioStream,
			requestPortfStream: reqStream,
			newDate() { return new Date("2015-09-05T12:47:50.725Z") },
			newUUID() { return "1111" }
		};
				
		var logSpy = sinon.spy(logger, "write");
		var pubSpy = sinon.spy(portfChangedOpts.pub, "write");

		tnConnectStream.concat(sub.stream.take(1)).subscribe(() => {
			traderNet.startRecievePortfolio();
		 	portfChanged.handle(portfChangedOpts);			 
		});

		setTimeout(() => {
			
			console.log(pubSpy.callCount);
			
			expect(pubSpy.callCount).eq(2);

			/*
			console.log(JSON.stringify(pubSpy.firstCall.args[0], null, 2));
			console.log(JSON.stringify(pubSpy.secondCall.args[0], null, 2));
      		console.log(JSON.stringify(pubSpy.thirdCall.args[0], null, 2));
      		console.log(JSON.stringify(pubSpy.getCall(3).args[0], null, 2));
			*/
			done();
		}, 3000);
	})

});		