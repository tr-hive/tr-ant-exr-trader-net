/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/trade-handler.d.ts"/>
import chai = require('chai'); 
import handler = require('../dist/trade-handler');
import rabbit = require('da-rabbitmq-rx');
import trAnt = require("tr-ant-utils");
import tn = require("da-trader-net-rx");
import Rx = require("rx");
var expect = chai.expect;
import sinon = require("sinon");
import sinonChai = require("sinon-chai");

chai.use(sinonChai);

import getEnvVar = trAnt.getEnvVar;

const TEST_IDX = 1;
const RABBIT_URI = getEnvVar("RABBIT_URI_TEST"); 
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS") + TEST_IDX; 
const RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS") + TEST_IDX;

const TRADER_NET_URL=getEnvVar("TRADER_NET_URL_TEST");
const TRADER_NET_API_KEY=getEnvVar("TRADER_NET_API_KEY_TEST");
const TRADER_NET_SEC_KEY=getEnvVar("TRADER_NET_SEC_KEY_TEST");

console.log(RABBIT_URI, RABBIT_QUEUE_NOTIFS, RABBIT_QUEUE_CMDS);

var expectedLogs = [
{
  "oper": "exe",
  "status": "start",
  "data": {
    "cmd": {
      "key": "1",
      "issuer": "tr-ant-cmr-random",
      "portfolio": "tr-ant-cmr-random",
      "date": "2015-09-05T12:47:50.725Z",
      "reason": "test",
      "trade": {
        "ticket": "SBER",
        "oper": "buy",
        "quantity": 10
      }
    }
  }
},
{
  "oper": "exe",
  "status": "success",
  "data": {
    "cmd": {
      "id": 1,
      "cmd": {
        "key": "1",
        "issuer": "tr-ant-cmr-random",
        "portfolio": "tr-ant-cmr-random",
        "date": "2015-09-05T12:47:50.725Z",
        "reason": "test",
        "trade": {
          "ticket": "SBER",
          "oper": "buy",
          "quantity": 10
        }
      }
    },
    "order": {
      "ticket": "SBER",
      "action": 1,
      "orderType": 1,
      "currency": 2,
      "quantity": 10,
      "userOrderId": "1"
    }
  }
}	
];

var expectedOrder = {
 ticket: 'SBER',
  action: 1,
  orderType: 1,
  currency: 2,
  quantity: 10,
  userOrderId: '1' 	
};
	 
describe("check insert buy trade",  () => {

	it("buy sber",  (done) => {
						
		//subscribe here and validate all publishing
		var subOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_CMDS};
		var sub = new rabbit.RabbitSub(subOpts); 
		sub.connect();	
		
		var cmd : trAnt.ICmd = {
			key : "1",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: "2015-09-05T12:47:50.725Z",
			reason: "test",
			trade : {
				ticket: "SBER", oper : "buy", quantity: 10
			}
		};

		var commandsStream = Rx.Observable.create<trAnt.ICmd>(observer => {
		   setTimeout(() => observer.onNext(cmd), 10);			
		});
				
		var logger  = {
			write(j) { console.log(JSON.stringify(j, null, 2)); }	
		};
				
		var logWrite = sinon.spy(logger, "write");

		var handlerOpts : handler.IHandlerOpts = {
			cmdStream: commandsStream,
      portfolio: "tr-ant-cmr-random",	
			logger: logger, 		
			putOrder: (order: tn.IPutOrderData) => console.log(order),			
			storage : {
				lock: (key: string) => Rx.Observable.return(true),
				insertKey: (key) => Rx.Observable.return(1)
			},
			newDate() {return new Date("2015-09-05T12:47:50.725Z")}	
		};
		
		var orderSpy = sinon.spy(handlerOpts, "putOrder");		 
									
		sub.stream.take(1).subscribe(() => {																				 
			handler.handle(handlerOpts);
		});
		
		setTimeout(() => { 						
			expect(logWrite).callCount(2);
			expect(logWrite.firstCall.args[0]).eql(expectedLogs[0]);						
			expect(logWrite.secondCall.args[0]).eql(expectedLogs[1]);
			
			expect(orderSpy).callCount(1);
			expect(orderSpy.firstCall.args[0]).eql(expectedOrder);
									
			done();
		}, 1500);			
	})
	
});		