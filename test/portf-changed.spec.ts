/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/trade-handler.d.ts"/>
/// <reference path="../dist/portf-changed-handler.d.ts"/>
import chai = require('chai');
import handler = require('../dist/trade-handler');
import portfChanged = require('../dist/portf-changed-handler');
import rabbit = require('da-rabbitmq-rx');
import trAnt = require("tr-ant-utils");
import tn = require("da-trader-net-rx");
import Rx = require("rx");
var expect = chai.expect;
import sinon = require("sinon");
import sinonChai = require("sinon-chai");
import mongo = require("da-mongo-rx");

chai.use(sinonChai);

import getEnvVar = trAnt.getEnvVar;

const TEST_IDX = 2;
const RABBIT_URI = getEnvVar("RABBIT_URI_TEST");
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS") + TEST_IDX;
const RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS") + TEST_IDX;

const TRADER_NET_URL = getEnvVar("TRADER_NET_URL_TEST");
const TRADER_NET_API_KEY = getEnvVar("TRADER_NET_API_KEY_TEST");
const TRADER_NET_SEC_KEY = getEnvVar("TRADER_NET_SEC_KEY_TEST");

const LOCK_COLL = "lock" + TEST_IDX;
const KEYS_COLL = "trade_keys" + TEST_IDX;

const MONGO_URI = getEnvVar("MONGO_URI_TEST");

console.log(RABBIT_URI, RABBIT_QUEUE_NOTIFS, RABBIT_QUEUE_CMDS);

const PORTF_START_QUANTITY = 30;

//TODO: Portf start quntity = 30 !!!
var expectedLogs = [
//after request portfolio 		
{
  "oper": "exe",
  "status": "start",
  "group": "notif",
  "data": {
    "portf": {
      "accounts": [
        {
          "availableAmount": 1002877.1,
          "currency": 2,
          "currencyRate": 1,
          "forecastIn": 0,
          "forecastOut": 0
        }
      ],
      "positions": [
        {
          "security": 230,
          "securityType": 1,
          "securityKind": 1,
          "price": 2254.219381,
          "quantity": 30,
          "currency": 2,
          "currencyRate": 1,
          "securityName": "Сбербанк",
          "securityName2": "Sberbank",
          "openPrice": 2254.21935,
          "marketPrice": 75.17
        }
      ]
    }
  }
},
{
  "oper": "exe",
  "status": "success",
  "group": "notif",
  "data": {
    "portf": {
      "accounts": [
        {
          "availableAmount": 1002877.1,
          "currency": 2,
          "currencyRate": 1,
          "forecastIn": 0,
          "forecastOut": 0
        }
      ],
      "positions": [
        {
          "security": 230,
          "securityType": 1,
          "securityKind": 1,
          "price": 2254.219381,
          "quantity": 30,
          "currency": 2,
          "currencyRate": 1,
          "securityName": "Сбербанк",
          "securityName2": "Sberbank",
          "openPrice": 2254.21935,
          "marketPrice": 75.17
        }
      ]
    },
    "notif": {
      "key": "1111",
      "type": "INotifPortfolioChanged",
      "issuer": "tr-ant-exr-trader-net",
      "date": "2015-09-05T12:47:50.725Z",
      "data": {
        "portfolio": null,
        "value": 1002877.1,
        "positions": [
          {
            "ticket": "SBER",
            "quantity": 30,
            "value": 2254.219381
          }
        ],
        "cmd": null
      }
    }
  }
},
{
  "oper": "exe",
  "status": "start",
  "data": {
    "cmd": {
      "key": "1",
      "issuer": "tr-ant-cmr-random",
      "portfolio": "tr-ant-cmr-random",
      "date": "2015-09-05T12:47:50.725Z",
      "reason": "test",
      "trade": {
        "ticket": "SBER",
        "oper": "buy",
        "quantity": 10
      }
    }
  }
},
{
  "oper": "exe",
  "status": "success",
  "data": {
    "cmd": {
      "id": 1,
      "cmd": {
        "key": "1",
        "issuer": "tr-ant-cmr-random",
        "portfolio": "tr-ant-cmr-random",
        "date": "2015-09-05T12:47:50.725Z",
        "reason": "test",
        "trade": {
          "ticket": "SBER",
          "oper": "buy",
          "quantity": 10
        }
      }
    },
    "order": {
      "ticket": "SBER",
      "action": 1,
      "orderType": 1,
      "currency": 2,
      "quantity": 10,
      "userOrderId": "1"
    }
  }
},
{
  "oper": "exe",
  "status": "start",
  "data": {
    "cmd": {
      "key": "2",
      "issuer": "tr-ant-cmr-random",
      "portfolio": "tr-ant-cmr-random",
      "date": "2015-09-05T12:47:50.725Z",
      "reason": "test",
      "trade": {
        "ticket": "SBER",
        "oper": "sell",
        "quantity": 10
      }
    }
  }
},
{
  "oper": "exe",
  "status": "success",
  "data": {
    "cmd": {
      "id": 2,
      "cmd": {
        "key": "2",
        "issuer": "tr-ant-cmr-random",
        "portfolio": "tr-ant-cmr-random",
        "date": "2015-09-05T12:47:50.725Z",
        "reason": "test",
        "trade": {
          "ticket": "SBER",
          "oper": "sell",
          "quantity": 10
        }
      }
    },
    "order": {
      "ticket": "SBER",
      "action": 3,
      "orderType": 1,
      "currency": 2,
      "quantity": 10,
      "userOrderId": "2"
    }
  }
},
{
  "oper": "exe",
  "status": "start",
  "group": "notif",
  "data": {
    "portf": {
      "accounts": [
        {
          "availableAmount": 1002125.8,
          "currency": 2,
          "currencyRate": 1,
          "forecastIn": 0,
          "forecastOut": 0
        }
      ],
      "positions": [
        {
          "security": 230,
          "securityType": 1,
          "securityKind": 1,
          "price": 3005.519381,
          "quantity": 40,
          "currency": 2,
          "currencyRate": 1,
          "securityName": "Сбербанк",
          "securityName2": "Sberbank",
          "openPrice": 3005.51932,
          "marketPrice": 75.16
        }
      ]
    }
  }
},
{
  "oper": "exe",
  "status": "success",
  "group": "notif",
  "data": {
    "portf": {
      "accounts": [
        {
          "availableAmount": 1002125.8,
          "currency": 2,
          "currencyRate": 1,
          "forecastIn": 0,
          "forecastOut": 0
        }
      ],
      "positions": [
        {
          "security": 230,
          "securityType": 1,
          "securityKind": 1,
          "price": 3005.519381,
          "quantity": 40,
          "currency": 2,
          "currencyRate": 1,
          "securityName": "Сбербанк",
          "securityName2": "Sberbank",
          "openPrice": 3005.51932,
          "marketPrice": 75.16
        }
      ]
    },
    "notif": {
      "key": "1111",
      "type": "INotifPortfolioChanged",
      "issuer": "tr-ant-exr-trader-net",
      "date": "2015-09-05T12:47:50.725Z",
      "data": {
        "portfolio": null,
        "value": 1002125.8,
        "positions": [
          {
            "ticket": "SBER",
            "quantity": 40,
            "value": 3005.519381
          }
        ],
        "cmd": null
      }
    }
  }
},
{
  "oper": "exe",
  "status": "start",
  "group": "notif",
  "data": {
    "portf": {
      "accounts": [],
      "positions": [
        {
          "security": 230,
          "securityType": 1,
          "securityKind": 1,
          "price": 2254.139536,
          "quantity": 30,
          "currency": 2,
          "currencyRate": 1,
          "securityName": "Сбербанк",
          "securityName2": "Sberbank",
          "openPrice": 2254.13949,
          "marketPrice": 75.12
        }
      ]
    }
  }
},
{
  "oper": "exe",
  "status": "success",
  "group": "notif",
  "data": {
    "portf": {
      "accounts": [],
      "positions": [
        {
          "security": 230,
          "securityType": 1,
          "securityKind": 1,
          "price": 2254.139536,
          "quantity": 30,
          "currency": 2,
          "currencyRate": 1,
          "securityName": "Сбербанк",
          "securityName2": "Sberbank",
          "openPrice": 2254.13949,
          "marketPrice": 75.12
        }
      ]
    },
    "notif": {
      "key": "1111",
      "type": "INotifPortfolioChanged",
      "issuer": "tr-ant-exr-trader-net",
      "date": "2015-09-05T12:47:50.725Z",
      "data": {
        "portfolio": null,
        "value": null,
        "positions": [
          {
            "ticket": "SBER",
            "quantity": 30,
            "value": 2254.139536
          }
        ],
        "cmd": null
      }
    }
  }
}	
];

var expectedOrders = [
{
  "key": "1111",
  "type": "INotifPortfolioChanged",
  "issuer": "tr-ant-exr-trader-net",
  "date": "2015-09-05T12:47:50.725Z",
  "data": {
    "portfolio": null,
    "value": 1002877.1,
    "positions": [
      {
        "ticket": "SBER",
        "quantity": 30,
        "value": 2254.219381
      }
    ],
    "cmd": null
  }
},
{
  "key": "1111",
  "type": "INotifPortfolioChanged",
  "issuer": "tr-ant-exr-trader-net",
  "date": "2015-09-05T12:47:50.725Z",
  "data": {
    "portfolio": null,
    "value": 1002877.1,
    "positions": [
      {
        "ticket": "SBER",
        "quantity": 30,
        "value": 2254.219381
      }
    ],
    "cmd": null
  }
}	
];

//THIS TEST IS BRITTLE, DEPENDS ON TRADER_NET PORTFTOLIO 
//DEFAULT VALUES AND SHOULD BE INVOKED MANUALLY 

describe.skip("portf changed test", () => {

	var db;
	before(done => {
		db = new mongo.MongoDb(MONGO_URI, [LOCK_COLL, KEYS_COLL]);
		Rx.Observable.merge(
			db.getCollection(LOCK_COLL).remove({}),
			db.getCollection(KEYS_COLL).remove({}))
			.subscribeOnCompleted(done);
	})

	it("buy / sell sber", (done) => {

		var traderNet = new tn.TraderNet(TRADER_NET_URL);
		var tnConnectStream = traderNet.connect({ apiKey: TRADER_NET_API_KEY, securityKey: TRADER_NET_SEC_KEY })

		tnConnectStream.subscribe(() =>
			console.log({ resource: "trader-net", oper: "connected", status: "success", opts: TRADER_NET_URL })
			, (err) => {
				console.log({ resource: "trader-net", status: "error", err: err, opts: TRADER_NET_URL });
			});
				
		//subscribe here and validate all publishing
		var subOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_CMDS };
		var sub = new rabbit.RabbitSub(subOpts);
		sub.connect();

		var notifsPubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_NOTIFS };
		var notifsPub = new rabbit.RabbitPub(notifsPubOpts); 
		notifsPub.connect();	

		var cmdBuy: trAnt.ICmd = {
			key: "1",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: "2015-09-05T12:47:50.725Z",
			reason: "test",
			trade: {
				ticket: "SBER", oper: "buy", quantity: 10
			}
		};

		var cmdSell: trAnt.ICmd = {
			key: "2",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: "2015-09-05T12:47:50.725Z",
			reason: "test",
			trade: {
				ticket: "SBER", oper: "sell", quantity: 10
			}
		};


		var commandsStream = Rx.Observable.create<trAnt.ICmd>(observer => {
			setTimeout(() => observer.onNext(cmdBuy), 10);
			setTimeout(() => observer.onNext(cmdSell), 800);
		});

		var logger = {
			write(j) { console.log(JSON.stringify(j, null, 2)); }
		};		

		var tradeHandlerOpts: handler.IHandlerOpts = {
			cmdStream: commandsStream,
			logger: logger,
      portfolio: "tr-ant-cmr-random",
			putOrder: (order: tn.IPutOrderData) => traderNet.putOrder(order),
			storage: {
				lock: (key: string) => db.lock(key, LOCK_COLL),
				insertKey: (key) => db.insertUniqueDocumentWithKey(key, KEYS_COLL)
			},
			newDate() { return new Date("2015-09-05T12:47:50.725Z") }
		};			
		
		var portfChangedOpts: portfChanged.IHandlerOpts = {			
			logger: logger,
			pub: notifsPub,
      portfolio: "tr-ant-cmr-random",
			portfChangedStream: traderNet.portfolioStream,
      requestPortfStream: Rx.Observable.never<any>(),
			newDate() { return new Date("2015-09-05T12:47:50.725Z") },
			newUUID() { return "1111" }
		};
		
		var logSpy = sinon.spy(logger, "write");
		var pubSpy = sinon.spy(portfChangedOpts.pub, "write");

		tnConnectStream.concat(sub.stream.take(1)).subscribe(() => {
			traderNet.startRecievePortfolio();
		 	portfChanged.handle(portfChangedOpts);
			handler.handle(tradeHandlerOpts);
		});

		setTimeout(() => {
      //initial portf + 4 buy / sell + 2 commands
      //expect(logSpy).callCount(7); 
      //???
			
			console.log(JSON.stringify(pubSpy.firstCall.args[0], null, 2));
			console.log(JSON.stringify(pubSpy.secondCall.args[0], null, 2));
      console.log(JSON.stringify(pubSpy.thirdCall.args[0], null, 2));
      console.log(JSON.stringify(pubSpy.getCall(3).args[0], null, 2));
      
      console.log(pubSpy.callCount);
			done();
		}, 3000);
	})

});		