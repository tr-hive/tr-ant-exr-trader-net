/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/trade-handler.d.ts"/>
import chai = require('chai'); 
import handler = require('../dist/trade-handler');
import rabbit = require('da-rabbitmq-rx');
import trAnt = require("tr-ant-utils");
import tn = require("da-trader-net-rx");
import Rx = require("rx");
var expect = chai.expect;
import sinon = require("sinon");
import sinonChai = require("sinon-chai");
import mongo = require("da-mongo-rx");

chai.use(sinonChai);

import getEnvVar = trAnt.getEnvVar;

const TEST_IDX = 1;
const RABBIT_URI = getEnvVar("RABBIT_URI_TEST"); 
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS") + TEST_IDX; 
const RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS") + TEST_IDX;

const TRADER_NET_URL=getEnvVar("TRADER_NET_URL_TEST");
const TRADER_NET_API_KEY=getEnvVar("TRADER_NET_API_KEY_TEST");
const TRADER_NET_SEC_KEY=getEnvVar("TRADER_NET_SEC_KEY_TEST");

const MONGO_URI = getEnvVar("MONGO_URI_TEST");

console.log(RABBIT_URI, RABBIT_QUEUE_NOTIFS, RABBIT_QUEUE_CMDS);

var expectedLogs = [
{
  "oper": "exe",
  "status": "start",
  "data": {
    "cmd": {
      "key": "1",
      "issuer": "tr-ant-cmr-random",
      "portfolio": "tr-ant-cmr-random",
      "date": "2015-09-05T12:47:50.725Z",
      "reason": "test",
      "trade": {
        "ticket": "SBER",
        "oper": "buy",
        "quantity": 10
      }
    }
  }
},
{
  "oper": "exe",
  "status": "success",
  "data": {
    "cmd": {
      "id": 1,
      "cmd": {
        "key": "1",
        "issuer": "tr-ant-cmr-random",
        "portfolio": "tr-ant-cmr-random",
        "date": "2015-09-05T12:47:50.725Z",
        "reason": "test",
        "trade": {
          "ticket": "SBER",
          "oper": "buy",
          "quantity": 10
        }
      }
    },
    "order": {
      "ticket": "SBER",
      "action": 1,
      "orderType": 1,
      "currency": 2,
      "quantity": 10,
      "userOrderId": "1"
    }
  }
},
{
  "oper": "exe",
  "status": "start",
  "data": {
    "cmd": {
      "key": "2",
      "issuer": "tr-ant-cmr-random",
      "portfolio": "tr-ant-cmr-random",
      "date": "2015-09-05T12:47:50.725Z",
      "reason": "test",
      "trade": {
        "ticket": "SBER",
        "oper": "sell",
        "quantity": 10
      }
    }
  }
},
{
  "oper": "exe",
  "status": "success",
  "data": {
    "cmd": {
      "id": 2,
      "cmd": {
        "key": "2",
        "issuer": "tr-ant-cmr-random",
        "portfolio": "tr-ant-cmr-random",
        "date": "2015-09-05T12:47:50.725Z",
        "reason": "test",
        "trade": {
          "ticket": "SBER",
          "oper": "sell",
          "quantity": 10
        }
      }
    },
    "order": {
      "ticket": "SBER",
      "action": 3,
      "orderType": 1,
      "currency": 2,
      "quantity": 10,
      "userOrderId": "2"
    }
  }
}	
];

var expectedOrders = [{
 ticket: 'SBER',
  action: 1,
  orderType: 1,
  currency: 2,
  quantity: 10,
  userOrderId: '1' 	
}, {
 ticket: 'SBER',
  action: 3,
  orderType: 1,
  currency: 2,
  quantity: 10,
  userOrderId: '2' 	
}
];
	 
describe("buy / sell whole routine",  () => {
  
  var db;
  before(done => {
    db = new mongo.MongoDb(MONGO_URI, ["lock", "trade_keys"]);
    Rx.Observable.merge(
      db.getCollection("lock").remove({}), 
      db.getCollection("trade_keys").remove({}))
    .subscribeOnCompleted(done);
  })

	it("buy / sell sber",  (done) => {
						
    var traderNet = new tn.TraderNet(TRADER_NET_URL);
    var tnConnectStream = traderNet.connect({apiKey: TRADER_NET_API_KEY, securityKey: TRADER_NET_SEC_KEY})
    
    tnConnectStream.subscribe(() =>
      console.log({resource: "trader-net", oper: "connected", status : "success", opts: TRADER_NET_URL}) 
    , (err) => {
	     console.log({resource: "trader-net", status : "error", err: err, opts: TRADER_NET_URL});	     
    });
            
		//subscribe here and validate all publishing
		var subOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_CMDS};
		var sub = new rabbit.RabbitSub(subOpts); 
		sub.connect();	        
		
		var cmdBuy : trAnt.ICmd = {
			key : "1",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: "2015-09-05T12:47:50.725Z",
			reason: "test",
			trade : {
				ticket: "SBER", oper : "buy", quantity: 10
			}
		};
    
    var cmdSell : trAnt.ICmd = {
			key : "2",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: "2015-09-05T12:47:50.725Z",
			reason: "test",
			trade : {
				ticket: "SBER", oper : "sell", quantity: 10
			}
		};


		var commandsStream = Rx.Observable.create<trAnt.ICmd>(observer => {
		   setTimeout(() => observer.onNext(cmdBuy), 10);			
       setTimeout(() => observer.onNext(cmdSell), 1000);
		});
				
		var logger  = {
			write(j) { console.log(JSON.stringify(j, null, 2)); }	
		};
				
		var logWrite = sinon.spy(logger, "write");

    var handlerOpts : handler.IHandlerOpts = {
      cmdStream: commandsStream,	
      logger: logger, 
      portfolio: "tr-ant-cmr-random",	
      putOrder: (order: tn.IPutOrderData) => traderNet.putOrder(order),
      storage : {
        lock: (key: string) => db.lock(key, "lock"),
        insertKey: (key) => db.insertUniqueDocumentWithKey(key, "trade_keys")
      },
      newDate() {return new Date("2015-09-05T12:47:50.725Z")}
    };
    		
		var orderSpy = sinon.spy(handlerOpts, "putOrder");		 
									
		tnConnectStream.concat(sub.stream.take(1)).subscribe(() => {																				 
			handler.handle(handlerOpts);
		});
		
		

		setTimeout(() => { 						
			expect(logWrite).callCount(4);
			expect(logWrite.firstCall.args[0]).eql(expectedLogs[0]);						
			expect(logWrite.secondCall.args[0]).eql(expectedLogs[1]);
			
			expect(orderSpy).callCount(2);
			expect(orderSpy.firstCall.args[0]).eql(expectedOrders[0]);
      expect(orderSpy.secondCall.args[0]).eql(expectedOrders[1]);
      									
			done();
		}, 2000);			
	})
	
});		