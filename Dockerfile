FROM node:0.12

ADD package.json /app/
ADD dist /app/dist

WORKDIR /app
RUN npm install

CMD ["npm", "start"]
